import com.atlassian.jira.component.ComponentAccessor

def issueManager = ComponentAccessor.getIssueManager()
def pluginAccessor = ComponentAccessor.getPluginAccessor()
def issuePropertiesManager = ComponentAccessor.getOSGiComponentInstanceOfType(pluginAccessor
       .getClassLoader().findClass("com.tse.jira.issueproperties.plugin.api.IssuePropertiesAOMgr"))

def issue = issueManager.getIssueByKeyIgnoreCase("DT-5")

//args: issueKey, propertyKey, propertyValue
//returns: propertyId
def propertyId = issuePropertiesManager
	.addIssueProperty(issue.getKey(), "car.name", "honda");