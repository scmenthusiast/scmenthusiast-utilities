import com.atlassian.jira.component.ComponentAccessor

def issueManager = ComponentAccessor.getIssueManager()
def pluginAccessor = ComponentAccessor.getPluginAccessor()
def issuePropertiesManager = ComponentAccessor.getOSGiComponentInstanceOfType(pluginAccessor
       .getClassLoader().findClass("com.tse.jira.issueproperties.plugin.api.IssuePropertiesAOMgr"))

def issue = issueManager.getIssueByKeyIgnoreCase("DT-5")

issuePropertiesManager.getIssuePropertiesByIssueId(issue.getId()).each {property ->
    log.warn(property.getPropertyKey() +" >> "+ property.getPropertyValue())
}