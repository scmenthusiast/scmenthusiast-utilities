# Generate JIRA Auth Token

Go to, profile > Personal Access Tokens > Create token.

# Fetch Execute
```
$ python issue_properties_fetch.py --issue-key DT-2878

----------------------------
propertyId: 2
propertyKey: jenkins.param.name
propertyValue: test34
lastUpdated: 1524122368000
----------------------------

Total no.of issue properties: 1
$
```

# Add Execute
```
$ python issue_properties_add.py --issue-key DT-2878 --property-key "build.version" --property-value "1.0"
{"property":222,"message":"[Info] Added Issue Property."}
$
```

# Update Execute
```
$ python issue_properties_update.py --issue-key DT-2878  --property-key "build.version" --property-value "2.0"
{"property":222,"message":"[Info] Updated Issue Property."}
$
```