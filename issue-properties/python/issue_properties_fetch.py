import requests
import argparse
import sys


class IssuePropertiesRead(object):
    def __init__(self):
        """
        __init__(). prepares or initiates configuration
        :return: None
        """
        self.jira_url = "http://localhost:8080"
        self.jira_token = "NDU2ODg0MDU0NTE3OoFY50ghzKdjXNmqPtzVYgXYNZnl"

    def run(self, issue_key):
        """
        run(). prepares or initiates configuration file
        :param issue_key: Script argument issue key
        :return: None
        """
        with requests.Session() as session:
            session.headers.update({'Content-Type': 'application/json'})
            session.headers.update({"Authorization": "Bearer {0}".format(self.jira_token)})
            jira_rest_url = "{0}{1}".format(self.jira_url, "/rest/issueproperties/1.0/property/list/?issueKey={0}".format(issue_key))
            response = session.get(jira_rest_url)
            if response.status_code == 200:
                issue_properties = response.json()
                for i in issue_properties:
                    print("----------------------------")
                    print("propertyId: {0}".format(i.get("propertyId")))
                    print("propertyKey: {0}".format(i.get("propertyKey")))
                    print("propertyValue: {0}".format(i.get("propertyValue")))
                    print("lastUpdated: {0}".format(i.get("lastUpdated")))
                    print("----------------------------")
                    print("")
                print("Total no.of issue properties: {0}".format(len(issue_properties)))
            else:
                print("Status: {0}".format(response.status_code))
                print(response.text)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """
    parser = argparse.ArgumentParser(description='Fetch JIRA Issue Properties Script')
    parser.add_argument("--issue-key", help='Required Issue Key', required=True)
    args = parser.parse_args()
    if args.issue_key:
        scm = IssuePropertiesRead()
        scm.run(args.issue_key)
    else:
        print('[usage] issue_properties_fetch.py --issue-key [issue key]')
        sys.exit(1)

if __name__ == "__main__":
    main()