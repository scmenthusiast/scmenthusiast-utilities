import requests
import argparse
import sys


class ProjectPropertiesRead(object):
    def __init__(self):
        """
        __init__(). prepares or initiates configuration
        :return: None
        """
        self.jira_url = "http://localhost:8080"
        self.jira_token = "NDU2ODg0MDU0NTE3OoFY50ghzKdjXNmqPtzVYgXYNZnl"

    def run(self, project_key):
        """
        run(). prepares or initiates configuration file
        :param project_key: Script argument project key
        :return: None
        """
        with requests.Session() as session:
            session.headers.update({'Content-Type': 'application/json'})
            session.headers.update({"Authorization": "Bearer {0}".format(self.jira_token)})
            jira_rest_url = "{0}{1}".format(self.jira_url, "/rest/projectproperties/1.0/property/list/?projectKey={0}".format(project_key))
            response = session.get(jira_rest_url)
            if response.status_code == 200:
                project_properties = response.json()
                for i in project_properties:
                    print("----------------------------")
                    print("propertyId: {0}".format(i.get("propertyId")))
                    print("propertyKey: {0}".format(i.get("propertyKey")))
                    print("propertyValue: {0}".format(i.get("propertyValue")))
                    print("lastUpdated: {0}".format(i.get("lastUpdated")))
                    print("----------------------------")
                    print("")
                print("Total no.of project properties: {0}".format(len(project_properties)))
            else:
                print("Status: {0}".format(response.status_code))
                print(response.text)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """
    parser = argparse.ArgumentParser(description='Fetch JIRA project Properties Script')
    parser.add_argument("--project-key", help='Required project Key', required=True)
    args = parser.parse_args()
    if args.project_key:
        scm = ProjectPropertiesRead()
        scm.run(args.project_key)
    else:
        print('[usage] project_properties_fetch.py --project-key [project key]')
        sys.exit(1)

if __name__ == "__main__":
    main()