import requests
import argparse
import sys


class ProjectPropertiesAdd(object):
    def __init__(self):
        """
        __init__(). prepares or initiates configuration
        :return: None
        """
        self.jira_url = "http://localhost:8080"
        self.jira_token = "NDU2ODg0MDU0NTE3OoFY50ghzKdjXNmqPtzVYgXYNZnl"

    def run(self, project_key, property_key, property_value):
        """
        run(). prepares or initiates configuration file
        :param project_key: Script argument project key
        :param property_key: Script argument property key
        :param property_value: Script argument property value
        :return: None
        """
        with requests.Session() as session:
            session.headers.update({'Content-Type': 'application/json'})
            session.headers.update({"Authorization": "Bearer {0}".format(self.jira_token)})
            jira_rest_url = "{0}{1}".format(self.jira_url, "/rest/projectproperties/1.0/property/add")
            property_data = {
                "projectKey": str(project_key),
                "propertyKey": str(property_key),
                "propertyValue": str(property_value)
            }
            response = session.post(jira_rest_url, json=property_data)
            if response.status_code == 200:
                print(response.text)
            else:
                print("Status: {0}".format(response.status_code))
                print(response.text)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """
    parser = argparse.ArgumentParser(description='Fetch JIRA Project Properties Script')
    parser.add_argument("--project-key", help='Required Project Key', required=True)
    parser.add_argument("--property-key", help='Required property Key', required=True)
    parser.add_argument("--property-value", help='Required property Value', required=True)
    args = parser.parse_args()
    if args.project_key and args.property_key and args.property_value:
        scm = ProjectPropertiesAdd()
        scm.run(args.project_key, args.property_key, args.property_value)
    else:
        print('[usage] project_properties_add.py --project-key [project key] --property-key [property key] '
              '--property-value [property value]')
        sys.exit(1)

if __name__ == "__main__":
    main()