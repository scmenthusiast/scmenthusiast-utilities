# Generate Jira Personal Access Token

Go to, profile > Personal Access Tokens > Create token.

# Fetch Execute
```
$ python project_properties_fetch.py --project-key DT

----------------------------
propertyId: 2
propertyKey: jenkins.param.name
propertyValue: test34
lastUpdated: 1524122368000
----------------------------

Total no.of project properties: 1
$
```

# Add Execute
```
$ python project_properties_add.py --project-key DT --property-key "build.version" --property-value "1.0"
{"property":222,"message":"[Info] Added Project Property."}
$
```

# Update Execute
```
$ python project_properties_update.py --project-key DT  --property-key "build.version" --property-value "2.0"
{"property":222,"message":"[Info] Updated Project Property."}
$
```