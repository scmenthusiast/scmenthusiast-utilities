import com.atlassian.jira.component.ComponentAccessor

def projectManager = ComponentAccessor.getProjectManager()
def pluginAccessor = ComponentAccessor.getPluginAccessor()
def projectPropertiesManager = ComponentAccessor.getOSGiComponentInstanceOfType(pluginAccessor
       .getClassLoader().findClass("com.tse.jira.projectproperties.plugin.api.ProjectPropertiesAOMgr"))

projectManager.getProjectObjects().each {project ->
    log.warn("Processing project properties - "+ project.key)

	//example to process for each project property
	projectPropertiesManager.getProjectPropertiesByProjectKey(project.key).each {property ->
		log.warn(property.getPropertyKey() +" >> "+ property.getPropertyValue())
	}

	//example to process if project and property key exists
	def property = projectPropertiesManager.getProjectPropertyByKey(project, "project.end.date")
	if(property != null) {
		log.warn("FOUND: "+ property.getPropertyKey() +" >> "+ property.getPropertyValue())
	}
}