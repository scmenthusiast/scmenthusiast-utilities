import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.Project

def projectManager = ComponentAccessor.getProjectManager()
def pluginAccessor = ComponentAccessor.getPluginAccessor()
def projectPropertiesManager = ComponentAccessor.getOSGiComponentInstanceOfType(pluginAccessor
       .getClassLoader().findClass("com.tse.jira.projectproperties.plugin.api.ProjectPropertiesAOMgr"))

Project project = projectManager.getProjectObjByKey("DT")

//args: project, propertyKey
//returns: property object
def propertyEntry = projectPropertiesManager.getProjectPropertyByKey(project, "release.name")

//args: projectid, propertyId
//returns: propertyId
def propertyId = projectPropertiesManager
	.removeProjectProperty(project.getId(), propertyEntry.getID())