import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.Project

def projectManager = ComponentAccessor.getProjectManager()
def pluginAccessor = ComponentAccessor.getPluginAccessor()
def projectPropertiesManager = ComponentAccessor.getOSGiComponentInstanceOfType(pluginAccessor
       .getClassLoader().findClass("com.tse.jira.projectproperties.plugin.api.ProjectPropertiesAOMgr"))

Project project = projectManager.getProjectObjByKey("DT")

//args: project, propertyKey
//returns: property Object
def propertyEntry = projectPropertiesManager.getProjectPropertyByKey(project, "release.name")

//args: projectKey, propertyId, propertyKey, propertyValue
//returns: propertyId
def propertyId = projectPropertiesManager
       .updateProjectProperty(project, propertyEntry.getID(), "release.name", "4x4x4x")